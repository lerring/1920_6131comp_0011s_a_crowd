# 1920-6131COMP-0011s A Crowd

##### IDE - Platform IO

##### Setup and Loop Methods File

- 1920_6131comp_011s_a_crowd/src/C++main.cpp

#### Basic Features

-  **A :** Power on self-test (relevant devices)
-  **B :** Read sensors at appropriate rate and indicate appropriate light and log change identifying the variable on MCU's serial debug port
-  **C :** Every 5s, concatenate a simple debug string (containing current/latest values for all sensor readings/statuses) and print this via the MCU’s serial/debug port.
-  **D :** Determine (e.g. a PIR sensor) if the monitored building is occupied. Building shoudl be considered vacant 10 minutes after the last movement, and should be considered occupied as soon as motion is detected
-  **E :** Readings must be stored every 5s in volatile memory. If a connected push-button is held down for ~1s, the system should cycle between output intervals [5s, 10s, 30s, 60s, 2mins, 5mins].
-  **F** : Transmit the volatile-stored readings at the regular output interval [though with a minimum interval of 30s – i.e. at most twice a minute] to a server for remote storage.

### Advanced Features

-  **G** : If the building is determined occupied, as per D, the system should sound an audible alert.The alert can be “snoozed” for 2m by tapping a push button following the alert. Must only snooze an existing alert. Alert statuses (including snooze) should be logged as per feature C, and at each status change.
-  **H**: Store readings in a plain-text file on an SD card. Readings should be timestamped with an offset from a given local epoch. Files are named as a time/datestamp provided when you communicate with the server in the feature F. To minimise flash wear, log files should be written only every 2 minutes. Only once a batch has been verified as stored can be deleted from volatile memory.

#### Show piece :star2: :star2: :star2:

-  **I**: OLED - temp, humidity and PIR status - switches off if room is unoccupied

### Software Versions

| Software version | Hard ware Device(s) |Feature|Comments|
|--|--|--|--|
| 1.0 | `DHT11` - GPIO 4 | A + first part of B, C|Includes writer for printing to the debug port + feature A for DHT11. From B prints out of readouts for temp and humidity (every 5 seconds (feature C) - writer will need adding to for feature B to print out the variable(s) that cause change to the LED colour and also after the PIR has been added|
|2.0|`DHT11` GPIO 4, `RGB LED` - GPIO 21, 22 and 23 | Features A, B and C |Features, A ,B, C and D|PIR reports motion in accordance with requirements for feature D, information added to debug String (feature C)|
|3.0|`DHT11` - GPIO 4, `RGB LED` - GPIO 21, 22 and 23, `PIR(HC-SR501)` - GPIO 19|Features, A ,B, C and D|PIR reports motion in accordance with requirements for feature D, information added to debug String (feature C)|
|4.0|`DHT11` - GPIO 4, `RGB LED` - GPIO 21, 22 and 23, `PIR(HC-SR501)` - GPIO 19|Features, A, B, C and D|Software update only, to implement observer pattern |
|5.0|`DHT11` - GPIO 4, `RGB LED` - GPIO 21, 22 and 23, `PIR(HC-SR501)` - GPIO 19, `CycleButton` - GPIO 02|Features, A, B, C, D and partial E|Add cycle button|
|6.0|`DHT11` - GPIO 4, `RGB LED` - GPIO 21, 22 and 23, `PIR(HC-SR501)` - GPIO 19, `CycleButton` - GPIO 02, `Buzzer` GPI014, `SnoozeButton` GPIO13|Features, A, B, C, D, E, G| Partial submit while investigating timing issue |
|7.0|`DHT11` - GPIO 4, `RGB LED` - GPIO Red 26, Green 33 and Blue 32, `PIR(HC-SR501)` - GPIO 16, `CycleButton` - GPIO 02, `Buzzer` GPI014, `SnoozeButton` GPI-O13, `SD card` pins MISO 19, SCK 18 CS 5, MOSI 23|Features, A, B, C, D, E, G, H|Write to SD card, nb: timing issues remain|
|8.0|`DHT11` - GPIO 4, `RGB LED` - GPIO Red 33, Green 34 and Blue 35, `PIR(HC-SR501)` - GPIO 16, `CycleButton` - GPIO 02, `Buzzer` GPI014, `SnoozeButton` GPIO13, `SD card` pins MISO 19, SCK 18 CS 5, MOSI 23| Features, A, B, C, D, E, G, H|Software update only - resolution of issues with buzzer and task scheduler library|
|9.0|`DHT11` - GPIO 4, `RGB LED` - GPIO Red 26, Green 33 and Blue 32, `PIR(HC-SR501)` - GPIO 16, `CycleButton` - GPIO 02, `Buzzer` GPI014, `SnoozeButton` - GPIO13, `SD card` pins MISO 19, SCK 18 CS 5, MOSI 23|Features, A, B, C, D, E, F G, H|Transmission of files to server,(F), added unix time stamp to filenaming for H , update of LED GPIO pins|
|10.0|`DHT11` - GPIO 4, `RGB LED` - GPIO Red 26, Green 33 and Blue 32, `PIR(HC-SR501)` - GPIO 34, `CycleButton` - GPIO 02, `Buzzer` GPI014, `SnoozeButton` - GPIO13, `SD card` pins MISO 19, SCK 18 CS 5, MOSI 23,`OLED` SCL GPIO22, SDA GPIO 21|Features, A, B, C, D, E, F G, H, I|Print temp, humidity and occupied if room is occupied, otherwise switches off |
|11.0|`DHT11` - GPIO 4, `RGB LED` - GPIO Red 26, Green 33 and Blue 32, `PIR(HC-SR501)` - GPIO 34, `CycleButton` - GPIO 02, `Buzzer` GPI014, `SnoozeButton` - GPIO13, `SD card` pins MISO 19, SCK 18 CS 5, MOSI 23,`OLED` SCL GPIO22, SDA GPIO 21 |Features, A, B, C, D, E, F G, H, I|Software update to platform IO format and re-organization of files|
|12.0|As above |As above|Software updates for buzzer|
|13.0|As above|As above|Software update - improved logging during warm up|
|14.0|As above|Task 8|Test LED logic |
|15.0|As above|Task 8|Test Buzzer logic |
|16.0|As above|Task 8|WiFi connections tests |
|17.0|As above|Task 8|Fault notification on warm up for PIR and DHT11 |
|18.0|As above|Task 8|In operation DHT11 fault reporting  |

#### Voltage for devices

| Device| Voltage|
|--|--|
|DHT11 | 3V |
|PIR |5V|
|SD Adapter|Neamah 3V, Lee and Emma 5V|
|OLED|5V|
|RGB , Buzzer and buttons| powered through ESP32

#### Libraries used

- :cat: Task Scheduler
- :hamster: SPI
- :dog: Wire
- :rabbit: Adafruit_SSD_1306
- :mouse: HTTP Client
- :wolf: WiFi Client Secure
- :panda_face: SD
- :bird: FS
- :koala: WiFi
- :tiger: DHT_sensor library_for_ESPX
