#include <Arduino.h>
#include <unity.h>

#include "warm_up/WarmUp.h"
#include "sensor/dht/DhtSensor.h"
#include "sensor/pir/PirSensor.h"
#include "test_warmup.h"

#define DHT_PIN 4
#define PIR_PIN 34
#define SD_CS_PIN 5

/**
 * Proof of concept shows warm up runs within 2 minutes
*/ 
void test_warmup(void){

	Writer writer = Writer(SD_CS_PIN);
    long timeStart = millis();
   	WarmUp warmUpper = WarmUp(writer,DhtSensor(DHT_PIN),PirSensor(PIR_PIN));
	warmUpper.warmUp();
    TEST_ASSERT_EQUAL((millis() - timeStart <= 120000), true);
}
/**
 * Proof of concept shows warm up runs within 2 minutes
*/ 
void test_warmUpDht(void){
    DhtSensor dht = DhtSensor(DHT_PIN);
    TEST_ASSERT_TRUE(dht.warmUp());
}

/**
 * Proof of concept shows warm up runs within 2 minutes
*/ 
void test_warmUpPir(void){
    PirSensor pir = PirSensor(PIR_PIN);
    TEST_ASSERT_TRUE(pir.warmUp());
}
