#include <Arduino.h>
#include <unity.h>
#include <DHTesp.h>

#include "sensor/led/LedDecider.h"
#include "events/Dht11Event.h"
#include "test_ledlogic.h"

//test conditions for temperature
void test_temperatureOKHigh(void){
    LedDecider ledDecider =  LedDecider();
	LedState result = ledDecider.determineTempState(23.0);
    TEST_ASSERT_EQUAL(result, OKAY);
}

void test_temperatureOKLow(void){
    LedDecider ledDecider =  LedDecider();
	LedState result = ledDecider.determineTempState(18.0);
    TEST_ASSERT_EQUAL(result, OKAY);
}

void test_temperatureWarningHigh(void) {
    LedDecider ledDecider =  LedDecider();  
    LedState result = ledDecider.determineTempState(27.0);
    TEST_ASSERT_EQUAL(result, WARNING);
}

void test_temperatureWarningLow(void) {
    LedDecider ledDecider =  LedDecider();  
    LedState result = ledDecider.determineTempState(16.0);
    TEST_ASSERT_EQUAL(result, WARNING);
}

void test_temperatureDangerHigh(void) {
    LedDecider ledDecider =  LedDecider();  
    LedState result = ledDecider.determineTempState(27.1);
    TEST_ASSERT_EQUAL(result, DANGER);
}

void test_temperatureDangerLow(void) {
    LedDecider ledDecider =  LedDecider();  
    LedState result = ledDecider.determineTempState(15.9);
    TEST_ASSERT_EQUAL(result, DANGER);
}

//test logic for humidity
void test_humidityOKHigh(void) {
    LedDecider ledDecider = LedDecider();
    LedState result = ledDecider.determineHumidityState(60.0);
    TEST_ASSERT_EQUAL(result, OKAY);
}

void test_humidityOKLow(void) {
    LedDecider ledDecider = LedDecider();
    LedState result = ledDecider.determineHumidityState(35.0);
    TEST_ASSERT_EQUAL(result, OKAY);
}

void test_humidityWarningHigh(void){
    LedDecider ledDecider = LedDecider();
    LedState result = ledDecider.determineHumidityState(75.0);
    TEST_ASSERT_EQUAL(result, WARNING);
}

void test_humidityWarningLow(void){
    LedDecider ledDecider = LedDecider();
    LedState result = ledDecider.determineHumidityState(25.0);
    TEST_ASSERT_EQUAL(result, WARNING);
}

void test_humidityDangerHigh(void) {
    LedDecider ledDecider = LedDecider();
    LedState result = ledDecider.determineHumidityState(76.0);
    TEST_ASSERT_EQUAL(result, DANGER);
}

void test_humdidityDangerLow(void) {
    LedDecider ledDecider = LedDecider();
    LedState result = ledDecider.determineHumidityState(24.0);
    TEST_ASSERT_EQUAL(result, DANGER); 
}

//test combined states 
void test_tempAndHumidityOK(void) {
    LedDecider ledDecider = LedDecider();
    TempAndHumidity tempandhumidity;
    tempandhumidity.temperature = 23;
    tempandhumidity.humidity = 35;
    Dht11Event event = Dht11Event(tempandhumidity);
    ledDecider.update(&event);
    LedState result = ledDecider.determineWorstState();
    TEST_ASSERT_EQUAL(result, OKAY);
}

void test_tempWarningAndHumidityOK(void) {
    LedDecider ledDecider = LedDecider();
    TempAndHumidity tempandhumidity;
    tempandhumidity.temperature = 27;
    tempandhumidity.humidity = 35;
    Dht11Event event = Dht11Event(tempandhumidity);
    ledDecider.update(&event);
    LedState result = ledDecider.determineWorstState();
    TEST_ASSERT_EQUAL(result, WARNING);
}

void test_tempOKAndHumidityWarning(void) {
    LedDecider ledDecider = LedDecider();
    TempAndHumidity tempandhumidity;
    tempandhumidity.temperature = 23;
    tempandhumidity.humidity = 25;
    Dht11Event event = Dht11Event(tempandhumidity);
    ledDecider.update(&event);
    LedState result = ledDecider.determineWorstState();
    TEST_ASSERT_EQUAL(result, WARNING);
}

void test_tempWarningAndHumidityWarning(void) {
    LedDecider ledDecider = LedDecider();
    TempAndHumidity tempandhumidity;
    tempandhumidity.temperature = 27;
    tempandhumidity.humidity = 25;
    Dht11Event event = Dht11Event(tempandhumidity);
    ledDecider.update(&event);
    LedState result = ledDecider.determineWorstState();
    TEST_ASSERT_EQUAL(result, WARNING);
}

void test_tempDangerAndHumidityOK(void) {
    LedDecider ledDecider = LedDecider();
    TempAndHumidity tempandhumidity;
    tempandhumidity.temperature = 15.9;
    tempandhumidity.humidity = 35;
    Dht11Event event = Dht11Event(tempandhumidity);
    ledDecider.update(&event);
    LedState result = ledDecider.determineWorstState();
    TEST_ASSERT_EQUAL(result, DANGER);
}

void test_tempOKAndHumidityDanger(void) {
    LedDecider ledDecider = LedDecider();
    TempAndHumidity tempandhumidity;
    tempandhumidity.temperature = 18;
    tempandhumidity.humidity = 76;
    Dht11Event event = Dht11Event(tempandhumidity);
    ledDecider.update(&event);
    LedState result = ledDecider.determineWorstState();
    TEST_ASSERT_EQUAL(result, DANGER);
}

void test_tempDangerAndHumidityDanger(void) {
    LedDecider ledDecider = LedDecider();
    TempAndHumidity tempandhumidity;
    tempandhumidity.temperature = 15.9;
    tempandhumidity.humidity = 76;
    Dht11Event event = Dht11Event(tempandhumidity);
    ledDecider.update(&event);
    LedState result = ledDecider.determineWorstState();
    TEST_ASSERT_EQUAL(result, DANGER); 
}

void test_tempWarningAndHumidityDanger(void) {
    LedDecider ledDecider = LedDecider();
    TempAndHumidity tempandhumidity;
    tempandhumidity.temperature = 27;
    tempandhumidity.humidity = 90;
    Dht11Event event = Dht11Event(tempandhumidity);
    ledDecider.update(&event);
    LedState result = ledDecider.determineWorstState();
    TEST_ASSERT_EQUAL(result, DANGER);
}

void test_tempDangerAndHumidityWarning(void) {
    LedDecider ledDecider = LedDecider();
    TempAndHumidity tempandhumidity;
    tempandhumidity.temperature = 35;
    tempandhumidity.humidity = 70;
    Dht11Event event = Dht11Event(tempandhumidity);
    ledDecider.update(&event);
    LedState result = ledDecider.determineWorstState();
    TEST_ASSERT_EQUAL(result, DANGER);
}
