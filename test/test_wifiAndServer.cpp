#include <Arduino.h>
#include <unity.h>
#include "HTTPClient.h"
#include "WiFiClientSecure.h"

#include "test_wifiAndServer.h"
#include "wifi/wifi.h"
#include "writer/Writer.h"


//to run tests add SSID and password to wifi.h
void test_wifiConnection (void) {
    long timeStart = millis();
    wifi wifii = wifi();
    wifii.initialiseWifi();
    TEST_ASSERT_EQUAL((millis() - timeStart <= 4000), true);
}

void test_loadTheTimes(void) {
    wifi wiffi = wifi();
    wiffi.initialiseWifi();
    int time = wiffi.loadTheTimes();
    TEST_ASSERT_EQUAL(time > 0, true);
}



