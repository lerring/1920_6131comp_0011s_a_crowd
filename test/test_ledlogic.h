
void test_temperatureOKHigh(void);
void test_temperatureOKLow(void);
void test_temperatureWarningHigh(void);
void test_temperatureWarningLow(void);
void test_temperatureDangerHigh(void);
void test_temperatureDangerLow(void);

void test_humidityOKHigh(void);
void test_humidityOKLow(void);
void test_humidityWarningHigh(void);
void test_humidityWarningLow(void);
void test_humidityDangerHigh(void);
void test_humdidityDangerLow(void);

void test_tempAndHumidityOK(void);
void test_tempWarningAndHumidityOK(void);
void test_tempOKAndHumidityWarning(void);
void test_tempWarningAndHumidityWarning(void);
void test_tempDangerAndHumidityOK(void);
void test_tempOKAndHumidityDanger(void);
void test_tempWarningAndHumidityDanger(void);
void test_tempDangerAndHumidityWarning(void);