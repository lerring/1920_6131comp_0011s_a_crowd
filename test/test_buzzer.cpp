#include <Arduino.h>
#include <unity.h>
#include "sensor/buzzer/BuzzerDecider.h"
#include "sensor/pir/PirState.h"
#include "sensor/led/LedState.h"
/**
*/ 
void test_decideBuzzOccupiedDanger(void){
    BuzzerDecider 	buzzerDecider = BuzzerDecider();
    buzzerDecider.setState(PirState::OCCUPIED);
    buzzerDecider.setState(LedState::DANGER);
    buzzerDecider.timeLastBuzzed=5000;
    TEST_ASSERT_TRUE(buzzerDecider.decideAlarm());
}

/**
*/ 
void test_decideBuzzOccupiedWarning(void){
    BuzzerDecider 	buzzerDecider = BuzzerDecider();
    buzzerDecider.setState(PirState::OCCUPIED);
    buzzerDecider.setState(LedState::WARNING);
    buzzerDecider.timeLastBuzzed=30000;
    TEST_ASSERT_TRUE(buzzerDecider.decideAlarm());
}

/**
*/ 
void test_decideBuzzOccupiedOkay(void){
    BuzzerDecider 	buzzerDecider = BuzzerDecider();
    buzzerDecider.setState(PirState::OCCUPIED);
    buzzerDecider.setState(LedState::OKAY);
    TEST_ASSERT_FALSE(buzzerDecider.decideAlarm());
}

/**
*/ 
void test_decideBuzzUnOccupied(void){
    BuzzerDecider 	buzzerDecider = BuzzerDecider();
    buzzerDecider.setState(PirState::UNOCCUPIED);
    buzzerDecider.setState(LedState::DANGER);
    TEST_ASSERT_FALSE(buzzerDecider.decideAlarm());
}

void test_decideBuzzSnoozed(void){
    BuzzerDecider 	buzzerDecider = BuzzerDecider();
    buzzerDecider.setState(PirState::OCCUPIED);
    buzzerDecider.setState(LedState::DANGER);
    buzzerDecider.timeLastBuzzed=5000;
    buzzerDecider.timeSnoozed=0;
    buzzerDecider.snoozedState=DANGER;
    TEST_ASSERT_FALSE(buzzerDecider.decideAlarm());
    buzzerDecider.timeSnoozed=120001;
    TEST_ASSERT_TRUE(buzzerDecider.decideAlarm());
}

void test_decideBuzzSnoozedChange(void){
    BuzzerDecider 	buzzerDecider = BuzzerDecider();
    buzzerDecider.setState(PirState::OCCUPIED);
    buzzerDecider.setState(LedState::DANGER);
    buzzerDecider.timeLastBuzzed=5000;
    buzzerDecider.timeSnoozed=0;
    buzzerDecider.snoozedState=DANGER;
    TEST_ASSERT_FALSE(buzzerDecider.decideAlarm());
    buzzerDecider.setState(LedState::OKAY);
    TEST_ASSERT_FALSE(buzzerDecider.decideAlarm());
}