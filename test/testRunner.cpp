#include <Arduino.h>
#include <unity.h>
#include "test_ledlogic.h"
#include "test_warmup.h"
#include "test_wifiAndServer.h"
#include "test_buzzer.h"

void setup(){
    delay(2000);
    UNITY_BEGIN();
}

void loop(){
    RUN_TEST(test_decideBuzzOccupiedDanger);
    RUN_TEST(test_decideBuzzOccupiedWarning);
    RUN_TEST(test_decideBuzzOccupiedOkay);
    RUN_TEST(test_decideBuzzUnOccupied);
    RUN_TEST(test_decideBuzzSnoozed);
    RUN_TEST(test_decideBuzzSnoozedChange);
    RUN_TEST(test_warmup);
    RUN_TEST(test_warmUpDht);
    RUN_TEST(test_warmUpPir);
    //Led states
    RUN_TEST(test_temperatureOKHigh);
    RUN_TEST(test_temperatureOKLow);
    RUN_TEST(test_temperatureWarningHigh);
    RUN_TEST(test_temperatureWarningLow);
    RUN_TEST(test_temperatureDangerHigh);
    RUN_TEST(test_temperatureDangerHigh);
    RUN_TEST(test_humidityOKHigh);
    RUN_TEST(test_humidityOKLow);
    RUN_TEST(test_humidityWarningHigh);
    RUN_TEST(test_humidityWarningLow);
    RUN_TEST(test_humidityDangerHigh);
    RUN_TEST(test_humdidityDangerLow);
    RUN_TEST(test_tempAndHumidityOK);
    RUN_TEST(test_tempWarningAndHumidityOK);
    RUN_TEST(test_tempOKAndHumidityWarning);
    RUN_TEST(test_tempWarningAndHumidityWarning);
    RUN_TEST(test_tempDangerAndHumidityOK);
    RUN_TEST(test_tempOKAndHumidityDanger);
    RUN_TEST(test_tempWarningAndHumidityDanger);
    RUN_TEST(test_tempDangerAndHumidityWarning);

    RUN_TEST(test_wifiConnection);
    RUN_TEST(test_loadTheTimes);


    UNITY_END();
}