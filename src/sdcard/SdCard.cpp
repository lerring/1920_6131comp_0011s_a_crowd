/*
 * SdCard.cpp
 *
 *  Created on: 1 Mar 2020
 *      Author: emma
 */

#include "SdCard.h"

SdCard::SdCard() {
}
SdCard::SdCard(int csPin, int offset) {
	SD.begin(csPin);
	this->offset = offset;
}

void SdCard::writeToSd(vector<String> &readings) {
	String filename = "/readings-";
	unsigned long int fileTime = millis() + offset;
	filename = filename + fileTime + ".txt";
	Serial.println("printing to " + filename);
	File file = SD.open(filename, FILE_WRITE);

	for (auto &reading : readings)
		file.println(reading);
	readings.clear();
	file.close();
}
