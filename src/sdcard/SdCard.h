/*
 * SdCard.h
 *
 *  Created on: 1 Mar 2020
 *      Author: emma
 */

#ifndef SDCARD_SDCARD_H_
#define SDCARD_SDCARD_H_

#include "SD.h"
#include "SPI.h"
#include <WString.h>
#include <vector>

using namespace std;

class SdCard {
public:
	SdCard(int csPin, int offset);
	SdCard();
	void writeToSd(vector<String> &readings);
private:
	int offset;
};

#endif /* SDCARD_SDCARD_H_ */
