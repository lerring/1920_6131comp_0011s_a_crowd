/*
 * ButtonEvent.h
 *
 *  Created on: 19 Feb 2020
 *      Author: lerring
 */

#ifndef EVENTS_BUTTONEVENT_H_
#define EVENTS_BUTTONEVENT_H_

#include "../sensor/button/ButtonState.h"
#include "Event.h"

class ButtonEvent : public Event {
public:
	ButtonState buttonState;
	ButtonEvent(ButtonState buttonState);
	virtual ~ButtonEvent();
    virtual int classType() { return EVENT_BUTTON; }
};

#endif /* EVENTS_BUTTONEVENT_H_ */
