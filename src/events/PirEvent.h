/*
 * PirEvent.h
 *
 *  Created on: 19 Feb 2020
 *      Author: lerring
 */

#ifndef EVENTS_PIREVENT_H_
#define EVENTS_PIREVENT_H_

#include "../sensor/pir/PirState.h"
#include "Event.h"

class PirEvent : public Event{
public:
	PirEvent(PirState pirState);
	virtual ~PirEvent();
	PirState pirState;
    virtual int classType() { return EVENT_PIR; }

};

#endif /* EVENTS_PIREVENT_H_ */
