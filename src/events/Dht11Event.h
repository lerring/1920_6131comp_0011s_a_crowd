/*
 * Dht11Event.h
 *
 *  Created on: 18 Feb 2020
 *      Author: lerring
 */

#ifndef EVENTS_DHT11EVENT_H_
#define EVENTS_DHT11EVENT_H_

#include <DHTesp.h>
#include "Event.h"
#include <algorithm>
#include <cmath>

class Dht11Event : public Event {
public:
	TempAndHumidity reading;
	Dht11Event(TempAndHumidity reading);
	virtual ~Dht11Event();
    virtual int classType() { return EVENT_DHT11; }
};

#endif /* EVENTS_DHT11EVENT_H_ */
