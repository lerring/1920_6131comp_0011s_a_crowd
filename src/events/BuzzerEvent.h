/*
 * BuzzerEvent.h
 *
 *  Created on: 3 Mar 2020
 *      Author: lerring
 */

#ifndef EVENTS_BUZZEREVENT_H_
#define EVENTS_BUZZEREVENT_H_

#include "Event.h"

class BuzzerEvent: public Event {
public:
	BuzzerEvent();
	BuzzerEvent(int duration);
	virtual ~BuzzerEvent();
    virtual int classType() { return EVENT_BUZZER; }
    int durationSeconds;
};

#endif /* EVENTS_BUZZEREVENT_H_ */
