/*
 * EventDefinition.h
 *
 *  Created on: 3 Mar 2020
 *      Author: lerring
 */

#ifndef EVENTS_EVENTDEFINITION_H_
#define EVENTS_EVENTDEFINITION_H_

#define EVENT 1
#define EVENT_LED 2
#define EVENT_BUTTON 3
#define EVENT_DHT11 4
#define EVENT_PIR 5
#define EVENT_BUZZER 6

#endif /* EVENTS_EVENTDEFINITION_H_ */
