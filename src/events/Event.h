/*
 * Event.h
 *
 *  Created on: 18 Feb 2020
 *      Author: lerring
 */

#ifndef EVENTS_EVENT_H_
#define EVENTS_EVENT_H_

#include "EventDefinition.h"

class Event {
public:
	Event();
	virtual ~Event();
	// https://arduino.stackexchange.com/questions/55157/dynamic-cast-not-permitted-with-fno-rtti
    virtual int classType() { return EVENT; }
};

#endif /* EVENTS_EVENT_H_ */
