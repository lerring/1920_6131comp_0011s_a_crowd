/*
 * LedEvent.h
 *
 *  Created on: 19 Feb 2020
 *      Author: lerring
 */

#ifndef EVENTS_LEDEVENT_H_
#define EVENTS_LEDEVENT_H_

#include "../sensor/led/LedState.h"
#include "Event.h"

class LedEvent : public Event{

public:
	LedState ledState;
	LedEvent(LedState ledState);
	virtual ~LedEvent();
    virtual int classType() { return EVENT_LED; }

};

#endif /* EVENTS_LEDEVENT_H_ */
