/*
 * Oled.cpp
 *
 *  Created on: 22 Mar 2020
 *      Author: emma
 */

#include "Oled.h"
#include "../events/Dht11Event.h"
#include "../events/PirEvent.h"

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

Oled::Oled() {}

void Oled::initialize() {
	if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x32
		Serial.println(F("SSD1306 allocation failed"));
		for (;;)
			;
	}
	delay(2000);
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setCursor(0, 0);
	display.display();
}

void Oled::update(Event *e) {

	if (e->classType() == EVENT_PIR) {
		PirEvent *pirEvent = static_cast<PirEvent*>(e);
		this->setState(pirEvent->pirState);
	}

	if (e->classType() == EVENT_DHT11) {
		Dht11Event *dht11Event = static_cast<Dht11Event*>(e);
		this->setReading(dht11Event->reading);
	}
}

void Oled::outputToOled() {
	display.clearDisplay();
	if (this->currPirState == OCCUPIED) {
		display.setCursor(0, 0);
		display.print("Temperature ");
		display.print(this->currReading.temperature);
		display.println(" C");
		display.print("Humidity    ");
		display.print(this->currReading.humidity);
		display.println(" %");
		display.print("Occupied    ");
		display.print(PirStateH::asString(this->currPirState));
	}
	display.display();
}

void Oled::setReading(TempAndHumidity newReading) {
	DhtObserver::setReading(newReading);
	outputToOled();
}

void Oled::setState(PirState newState) {
	PirObserver::setState(newState);
	// outputToOled();
}
