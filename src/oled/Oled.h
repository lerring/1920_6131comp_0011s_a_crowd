/*
 * Oled.h
 *
 *  Created on: 22 Mar 2020
 *      Author: emma
 */

#ifndef OLED_OLED_H_
#define OLED_OLED_H_

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_SSD1306.h>
#include "../sensor/dht/DhtObserver.h"
#include "../sensor/pir/PirObserver.h"

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels
#define OLED_RESET -1

class Oled: public DhtObserver, public PirObserver {
	public:
		Oled();
		void update(Event* e);
		void setReading(TempAndHumidity newReading);
		void setState(PirState newState);
		void initialize();

	private:
		void outputToOled();
};

#endif /* OLED_OLED_H_ */
