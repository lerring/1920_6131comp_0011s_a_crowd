/*
 * Subject.h
 *
 *  Created on: 13 Feb 2020
 *      Author: lerring
 */

#ifndef SUBJECT_H_
#define SUBJECT_H_

#include <vector>
#include "IObserver.h"
#include "../events/Event.h"

class Subject {

public:
	std::vector<IObserver*> observers;
	void attachObserver(IObserver *observer);
	void detachObserver(IObserver *observer);
    void notifyObservers(Event *val);
};

#endif /* SUBJECT_H_ */
