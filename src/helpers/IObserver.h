/*
 * IObserver.h
 *
 *  Created on: 13 Feb 2020
 *      Author: lerring
 */

#ifndef IOBSERVER_H_
#define IOBSERVER_H_

#include "../events/Event.h"
#include "Arduino.h"

class IObserver
{
public:
    virtual void update(Event* e) = 0;
	bool timeDiff(unsigned long lastChangeTime, int specifiedDelay){
			return (millis() - lastChangeTime >= specifiedDelay);
	}
};

#endif /* IOBSERVER_H_ */
