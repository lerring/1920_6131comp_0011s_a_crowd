/*
 * GodObserver.cpp
 *
 *  Created on: 3 Mar 2020
 *      Author: lerring
 */

#include "GodObserver.h"

GodObserver::GodObserver() {
}

GodObserver::~GodObserver() {
}

void GodObserver::setState(ButtonState newState) {
	ButtonObserver::setState(newState);
}

void GodObserver::setState(LedState newState){
	LedObserver::setState(newState);
}

void GodObserver::setState(PirState newState){
	PirObserver::setState(newState);
}

void GodObserver::setReading(TempAndHumidity newReading){
	DhtObserver::setReading(newReading);
}

/*
 * Acts as a router for events it has recieved to bypass diamond dependencies
 */
void GodObserver::update(Event* e){

	if (e->classType() == EVENT_BUTTON)
		ButtonObserver::update((ButtonEvent*) e);

	if (e->classType() == EVENT_PIR)
		PirObserver::update((PirEvent*) e);

	if (e->classType() == EVENT_LED)
		LedObserver::update((LedEvent*) e);

	if (e->classType() == EVENT_DHT11)
		DhtObserver::update((Dht11Event*) e);

}
