/*
 * GodObserver.h
 *
 *  Created on: 3 Mar 2020
 *      Author: lerring
 */

#ifndef GODOBSERVER_H_
#define GODOBSERVER_H_

#include "../sensor/button/ButtonObserver.h"
#include "../sensor/button/ButtonState.h"
#include "../sensor/dht/DhtObserver.h"
#include "../sensor/led/LedObserver.h"
#include "../sensor/led/LedState.h"
#include "../sensor/pir/PirObserver.h"
#include "../sensor/pir/PirState.h"

#include <DHTesp.h>
#include "../events/ButtonEvent.h"
#include "../events/Event.h"
#include "../events/EventDefinition.h"
#include "../events/LedEvent.h"
#include "../events/PirEvent.h"
#include "../events/Dht11Event.h"

class GodObserver : public ButtonObserver, public PirObserver, public LedObserver, public DhtObserver
{
public:
	GodObserver();
	virtual ~GodObserver();
	void update(Event* newState);
    void setState(ButtonState newState);
    void setState(PirState newState);
    void setState(LedState newState);
    void setReading(TempAndHumidity newReading);
};

#endif /* GODOBSERVER_H_ */
