/*
 * Subject.cpp
 *
 *  Created on: 13 Feb 2020
 *      Author: lerring
 */

#include "Subject.h"

#include <algorithm>
#include <iterator>

using namespace std;

void Subject::attachObserver(IObserver *observer)
{
    observers.push_back(observer);
}

void Subject::detachObserver(IObserver *observer)
{
    observers.erase(std::remove(observers.begin(), observers.end(), observer), observers.end());
}


void Subject::notifyObservers(Event* e)
{
    for(vector<IObserver*>::const_iterator iter = observers.begin(); iter != observers.end(); ++iter)
    {
        if(*iter != 0)
        {
            (*iter)->update(e);
        }
    }
}
