/*
 * WarmUp.cpp
 *
 *  Created on: 6 Feb 2020
 *      Author: lerring
 */
#include "WarmUp.h"

WarmUp::WarmUp(Writer writer, DhtSensor dht, PirSensor pir){
	this->writer=writer;
	this->dht = dht;
	this->pir = pir;
};

void WarmUp::warmUp(){
	Serial.println("DHT Self Test!");
	if(!this->dht.warmUp())
		writer.reportError("DHT warmup failed");
	Serial.println("PIR Self Test!");
	if(!this->pir.warmUp())
		writer.reportError("PIR warmup failed");
	Serial.println("Tests Complete!");
}