/*
 * WarmUp.h
 *
 *  Created on: 6 Feb 2020
 *      Author: lerring
 */

#include "../sensor/dht/DhtSensor.h"
#include "../sensor/pir/PirSensor.h"
#include "../writer/Printer.h"
#include "../writer/Writer.h"
#ifndef WARMUP_H_
#define WARMUP_H_

class WarmUp{

public:
	WarmUp(Writer writer, DhtSensor dht, PirSensor pir);
	void warmUp();
private:
	DhtSensor dht;
	PirSensor pir;
	Writer writer;
};

#endif /* WARMUP_H_ */
