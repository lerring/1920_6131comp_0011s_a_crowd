#ifndef UNIT_TEST

#define _TASK_STD_FUNCTION   // Compile with support for std::function
#define _TASK_00_CALLBACKS
#define _TASK_TIMEOUT

#include <TaskScheduler.h>

#include <Arduino.h>

#include "sensor/buzzer/Buzzer.h"
#include "sensor/button/Button.h"
#include "sensor/buzzer/BuzzerDecider.h"
#include "sensor/dht/DhtSensor.h"
#include "sensor/led/Led.h"
#include "sensor/led/LedDecider.h"
#include "sensor/pir/PirSensor.h"
#include "writer/Printer.h"
#include "writer/Writer.h"
#include "oled/Oled.h"
#include "warm_up/WarmUp.h"

// LED PINs
#define LED_PIN_R   26
#define LED_PIN_G   33
#define LED_PIN_B   32
// BUZZER PIN
#define BUZZER_PIN 12
// BUTTONS
#define CYCLE_BUTTON_PIN 2
#define SNOOZE_BUTTON_PIN 13
// SENSORS
#define DHT_PIN 4
#define PIR_PIN 34
#define SD_CS_PIN 5

DhtSensor dhtSensor;
PirSensor pirSensor;

Button cycleButton;
Button snoozeButton;

Led led;
BuzzerDecider buzzerDecider;
LedDecider ledDecider;
Writer writer;
Oled oled;

Buzzer* alarmBuzzer;

DhtObserver dhtObserver;
PirObserver pirObserver;

void readDht();
Task readDHT11Task(2000, TASK_FOREVER, &readDht);

void readPir();
Task readPirTask(1000, TASK_FOREVER, &readPir);

void readButtons();
Task readButtonTask(50, TASK_FOREVER, &readButtons);

void writeToVolatile();
Task writeToVolatileTask(500, TASK_FOREVER, &writeToVolatile);

void writeToSerial();
Task writeToSerialTask(5000, TASK_FOREVER, &writeToSerial);

void writeToSD();
Task writeToSDTask(20000, TASK_FOREVER, &writeToSD);

void writeToServer();
Task writeToServerTask(30000, TASK_FOREVER, &writeToServer);

void checkAlarm();
Task checkAlarmTask(1000, TASK_FOREVER, &checkAlarm);

void buzzExecutor();
Task executeBuzzTask(100, TASK_FOREVER, &buzzExecutor);

Scheduler runner;

void setup() {
	Serial.begin(115200);

	// SENSORS
	dhtSensor=DhtSensor(DHT_PIN);
	pirSensor=PirSensor(PIR_PIN);

	// INPUTS
	cycleButton=Button(CYCLE_BUTTON_PIN);
	snoozeButton=Button(SNOOZE_BUTTON_PIN);

	// OUTPUTS
	led=Led(LED_PIN_R, LED_PIN_G, LED_PIN_B);

	//
	alarmBuzzer = new Buzzer(BUZZER_PIN);
	//

	// OBSERVERS
	writer = Writer(SD_CS_PIN);
	buzzerDecider = BuzzerDecider();
	ledDecider = LedDecider();
	oled.initialize();

	dhtSensor.attachObserver(&ledDecider);
	dhtSensor.attachObserver(&writer);
	dhtSensor.attachObserver(&oled);

	pirSensor.attachObserver(&writer);
	pirSensor.attachObserver(&buzzerDecider);
	pirSensor.attachObserver(&oled);

	ledDecider.attachObserver(&led);
	buzzerDecider.attachObserver(alarmBuzzer);
	led.attachObserver(&writer);
	led.attachObserver(&buzzerDecider);

	cycleButton.attachObserver(&writer);

	snoozeButton.attachObserver(&buzzerDecider);

	WarmUp warmUpper = WarmUp(writer,dhtSensor,pirSensor);
	warmUpper.warmUp();

	//Setup Tasks
	runner.init();
	Printer::print("Initialised Scheduler");

	runner.addTask(readDHT11Task);
	runner.addTask(readPirTask);
	runner.addTask(readButtonTask);
	runner.addTask(writeToVolatileTask);
	runner.addTask(writeToSerialTask);
	runner.addTask(writeToSDTask);
	runner.addTask(writeToServerTask);
	runner.addTask(checkAlarmTask);
	runner.addTask(executeBuzzTask);
	Printer::print("Added tasks");

	//runner.enableAll(true);

	readDHT11Task.enable();
	readPirTask.enable();
	readButtonTask.enable();
	writeToVolatileTask.enable();
	writeToSerialTask.enable();
	writeToServerTask.enable();
	writeToSDTask.enable();
	checkAlarmTask.enable();
	executeBuzzTask.enable();

	Printer::print("Enabled Tasks");
}

void writeToVolatile(){
	writer.writeToVolatile();
}

void writeToSerial(){
       writer.writeToSerial();
}
void writeToSD() {
	writer.writeToSd();
}

void writeToServer(){
	writer.writeToServer();
}

void readDht(){
	dhtSensor.readSensor();
}

void readPir(){
	pirSensor.readSensor();
}

void readButtons(){
	cycleButton.readButton();
	snoozeButton.readButton();
}

void checkAlarm(){
	buzzerDecider.alarm();
}

/*
 * Buzzer needs individual thread to manage its executor and scheduler
 */
void buzzExecutor(){
	alarmBuzzer->executeTask();
}

void loop() {
	runner.execute();
}


#endif