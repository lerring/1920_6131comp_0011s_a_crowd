/*
 * wifi.cpp
 *
 *  Created on: 11 Mar 2020
 *      Author: emma
 */

#include "wifi.h"
#include <sstream>
#include <iterator>

wifi::wifi() {
}

bool wifi::initialiseWifi() {
	Serial.print("Connecting to ");  
	Serial.println(SSID);
	WiFi.begin(SSID, PASS);
	while (WiFi.status() != WL_CONNECTED) {
		delay(250);
		Serial.println(".");
	}
	Serial.print("Connected as :");
	Serial.println(WiFi.localIP());
	return true;
}


int wifi::loadTheTimes() {
	String url = "/api/time";
	String fullURL = "http://";
	fullURL.concat(HOST);
	fullURL.concat(url);
	HTTPClient hclient;
	hclient.begin(fullURL);
	int retCode = hclient.GET();
	long time = 0;
	if (retCode == HTTP_CODE_OK) {
		String body = hclient.getString();
		time = body.toInt();
	} else {
		Serial.println("Error");
		Serial.println(HTTPClient::errorToString(retCode));
	}
	hclient.end();
	return time;
}

void wifi::reportError(unsigned long faultTime, String macAddr, String description) {
		HTTPClient hclient;
		String url = "/api/faults";
		String fullURL = "http://";
		fullURL.concat(HOST);
		fullURL.concat(url);
		hclient.begin(fullURL); //Specify destination for HTTP request
		hclient.addHeader("Content-Type", "application/json");
		String vts = "[{";
		vts.concat(" \"fault_time\": "+String(faultTime)+",");
		vts.concat(" \"mac_address\": \""+macAddr+"\",");
		vts.concat(" \"description\": \""+description+"\"");
		vts.concat("}]");
		Serial.println(vts);
		int httpResponseCode = hclient.POST(vts); //Send the POST request
		Serial.print("response code ");
		Serial.println(httpResponseCode);
}

void wifi::sendToServer(vector<String> serverQueue) {
	if (!serverQueue.empty()) {
		HTTPClient hclient;
		String url = "/api/measurements";
		String fullURL = "http://";
		fullURL.concat(HOST);
		fullURL.concat(url);

		hclient.begin(fullURL); //Specify destination for HTTP request
		hclient.addHeader("Content-Type", "application/json");
		String vts = "{ \"data\": [";
		// copy all but the last element to avoid a trailing newline
		for (int i = 0; i < serverQueue.size() - 1; i++) {
			vts.concat(serverQueue[i]);
			vts.concat(", ");
		}
		// Add the last element with no delimiter
		vts.concat(serverQueue.back());
		vts.concat("]}");
		int httpResponseCode = hclient.POST(vts); //Send the POST request
		if (httpResponseCode == 201) {
			Serial.println(
				"Uploaded to server successfully");
		} else {
			Serial.print("Failed to upload to server with response code ");
			Serial.println(httpResponseCode);
		}
	}
}