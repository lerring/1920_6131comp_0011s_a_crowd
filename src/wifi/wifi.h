/*
 * wifi.h
 *
 *  Created on: 11 Mar 2020
 *      Author: emma
 */

#ifndef WIFI_WIFI_H_
#define WIFI_WIFI_H_

#include "Arduino.h"
#include "HTTPClient.h"
#include "WiFiClientSecure.h"
#include <vector>

using namespace std;

class wifi {

public:
	wifi();
	bool initialiseWifi();
	int loadTheTimes();
	void sendToServer(vector<String>);
	void reportError(unsigned long faultTime, String macAddr, String description);
private:
	const char* SSID = "SSID";
	const char* PASS = "PASS";
	const char *HOST = "ec2-18-188-223-197.us-east-2.compute.amazonaws.com:3003";
	
};

#endif /* WIFI_WIFI_H_ */
