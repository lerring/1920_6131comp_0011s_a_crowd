/*
 * Buzzer.h
 *
 *  Created on: 3 Mar 2020
 *      Author: lerring
 */

#ifndef SENSOR_BUZZER_H_
#define SENSOR_BUZZER_H_


#include "Arduino.h"
#include "../../events/Event.h"
#include "../../helpers/IObserver.h"
#include "../../events/BuzzerEvent.h"


#define _TASK_STD_FUNCTION   // Compile with support for std::function
#define _TASK_00_CALLBACKS
#define _TASK_TIMEOUT
#include <TaskSchedulerDeclarations.h>

class Buzzer : virtual public IObserver
{

public:
	Buzzer(int pin){
		pinMode(pin, OUTPUT);
		this->setPin(pin);
		runner = new Scheduler();
		buzzTask = new Task();

		buzzTask->set(
				//check timeout every 100ms
				100,
				//run untill timeout
				TASK_FOREVER,
				//callback
				[this](){
					return;
				},
				//on enable
				[this]() {
					this->startBuzz();
					return true;
				},
				// on disable
				[this]() {
					this->stopBuzz();
				}
		);
		this->runner->addTask(*buzzTask);

	}
	virtual ~Buzzer(){
	}

	void update(Event* newState);

	void startBuzz();
	void stopBuzz();
	void setPin(int pin);
	int pin;

	Scheduler* runner;
	Task* buzzTask;

	void executeTask();
};

#endif /* SENSOR_BUZZER_H_ */
