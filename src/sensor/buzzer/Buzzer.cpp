/*
 * Buzzer.cpp
 *
 *  Created on: 3 Mar 2020
 *      Author: lerring
 */

#include "Buzzer.h"

void Buzzer::setPin(int pin){
	this->pin = pin;
}

void Buzzer::startBuzz() {
	digitalWrite(this->pin, HIGH);
}

void Buzzer::stopBuzz(){
	digitalWrite(this->pin, LOW);
}

void Buzzer::executeTask(){
	if (this->buzzTask->isEnabled())
			this->runner->execute();
}

void Buzzer::update(Event* e){
	BuzzerEvent *buzzerEvent = static_cast<BuzzerEvent*>(e);
	this->buzzTask->setTimeout( buzzerEvent->durationSeconds * 1000, true);
	this->buzzTask->enable();
}
