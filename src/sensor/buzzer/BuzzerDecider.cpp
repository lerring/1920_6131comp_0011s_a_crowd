/*
 * BuzzerDecider.cpp
 *
 *  Created on: 3 Mar 2020
 *      Author: lerring
 */

#include "BuzzerDecider.h"

BuzzerDecider::BuzzerDecider() {
}

BuzzerDecider::~BuzzerDecider() {
}

void BuzzerDecider::buzzerEvent(){
	BuzzerEvent buzzEvent = BuzzerEvent(2);
	this->notifyObservers(&buzzEvent);
	this->timeLastBuzzed = millis();
}

void BuzzerDecider::setState(ButtonState newState){
	this->GodObserver::setState(newState);
	switch (this->currentButtonState){
		case ON:
			Serial.println("Snooze button pressed!");
			// if not currently snoozed
			if (snoozedState == UNSET){
				// if snooze is due to warning or danger alert
				if (this->currentLedState == WARNING or this->currentLedState == DANGER){
					this->snoozedState = this->currentLedState;
					this->timeSnoozed = millis();
				}
			}
			break;

		case OFF:
			break;
	}
	this->alarm();
}

void BuzzerDecider::setState(LedState newState){
	this->GodObserver::setState(newState);
	this->alarm();
}

void BuzzerDecider::setState(PirState newState){
	this->GodObserver::setState(newState);
	this->alarm();
}


boolean BuzzerDecider::decideAlarm(){
	// Once 2 minutes has elapsed
	// reset snooze
	if (this->IObserver::timeDiff(timeSnoozed, 120000) and snoozedState != UNSET){
		Serial.println("Snooze Reset!");
		snoozedState = UNSET;
	}
	if (this->currPirState == UNOCCUPIED)
		return false;

	switch(this->currentLedState) {
		case DANGER:
			// if currently snoozed from a danger alert
			if (snoozedState == DANGER)
				break;
			// 5 seconds haven't passed
			if (!timeDiff(timeLastBuzzed, 5000))
				break;
			return true;
		case WARNING:
			// if currently snoozed from a warning alert
			if (snoozedState == WARNING)
				break;
			// 30 seconds haven't passed
			if (!timeDiff(timeLastBuzzed, 30000))
				break;
			return true;
		case OKAY:
			this->snoozedState = UNSET;
			break;
		case UNSET:
			break;
	}
	return false;
}


void BuzzerDecider::alarm() {
	if (decideAlarm())
		this->buzzerEvent();
	
}
