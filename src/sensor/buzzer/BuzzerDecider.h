/*
 * BuzzerDecider.h
 *
 *  Created on: 3 Mar 2020
 *      Author: lerring
 */

#ifndef SENSOR_BUZZER_BUZZERDECIDER_H_
#define SENSOR_BUZZER_BUZZERDECIDER_H_

#include "../../helpers/GodObserver.h"
#include "../../helpers/Subject.h"
#include "../../events/BuzzerEvent.h"
#include "../pir/PirState.h"
#include "Arduino.h"
#include "Buzzer.h"

class BuzzerDecider : public GodObserver, public Subject {
public:
	BuzzerDecider();
	virtual ~BuzzerDecider();
	void alarm();
	boolean decideAlarm();
	void setState(ButtonState newState);
	void setState(LedState newState);
	void setState(PirState newState);
	long timeLastBuzzed = millis();
	long timeSnoozed;
	LedState snoozedState = UNSET;
private:
	void buzzerEvent();
};

#endif /* SENSOR_BUZZER_BUZZERDECIDER_H_ */
