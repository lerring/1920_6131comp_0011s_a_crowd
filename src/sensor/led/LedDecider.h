/*
 * LedDecider.h
 *
 *  Created on: 3 Mar 2020
 *      Author: lerring
 */

#ifndef SENSOR_LED_LEDDECIDER_H_
#define SENSOR_LED_LEDDECIDER_H_

#include "../../helpers/Subject.h"
#include "../dht/DhtObserver.h"
#include "LedState.h"

class LedDecider : public DhtObserver, public Subject {
public:
	LedDecider();
	virtual ~LedDecider();
	void update(Event* e);
	LedState determineTempState(float temp);
	LedState determineHumidityState(float humidity);
	LedState determineWorstState();

private:
	LedState tempState;
	LedState humidityState;
	void setHumidity(LedState newState);
	void setTemp(LedState newState);
};

#endif /* SENSOR_LED_LEDDECIDER_H_ */
