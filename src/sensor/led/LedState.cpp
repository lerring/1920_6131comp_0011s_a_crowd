/*
 * SystemState.cpp
 *
 *  Created on: 3 Mar 2020
 *      Author: lerring
 */

#include "LedState.h"

String LedStateH::asString(LedState state){
	switch(state){

	case OKAY:
		return "Okay";

	case WARNING:
		return "Warning";

	case DANGER:
		return "Danger";

	case UNSET:
		return "Unset";

	default:
		return "";

	}
}


