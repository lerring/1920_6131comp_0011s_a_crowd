/*
 * LedObserver.h
 *
 *  Created on: 3 Mar 2020
 *      Author: lerring
 */

#ifndef SENSOR_LED_LEDOBSERVER_H_
#define SENSOR_LED_LEDOBSERVER_H_

#include "../../helpers/IObserver.h"
#include "LedState.h"

class LedObserver : virtual public IObserver {
public:
	LedObserver();
	virtual ~LedObserver();
	void update(Event* newState);
protected:
	LedState currentLedState;
	virtual void setState(LedState newState);
};

#endif /* SENSOR_LED_LEDOBSERVER_H_ */
