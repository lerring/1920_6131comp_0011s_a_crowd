/*
 * Led.h
 *
 *  Created on: 29 Jan 2020
 *      Author: lerring
 */

#ifndef LED_H_
#define LED_H_

#include "../../helpers/IObserver.h"
#include "../../helpers/Subject.h"
#include "LedState.h"

#include <esp32-hal-ledc.h>
#include <cstdint>

#include "../../events/LedEvent.h"

class Led: public Subject, public IObserver {
public:
	Led();
	Led(int redPin, int greenPin, int bluePin);
	Led(int redPin, int greenPin, int bluePin, int rChannel, int gChannel,
			int bChannel, int timer, int baseFreq);
	void update(Event *newState);
private:
	void changeColour(LedState s);
};

#endif /* LED_H_ */
