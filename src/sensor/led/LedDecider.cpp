/*
 * LedDecider.cpp
 *
 *  Created on: 3 Mar 2020
 *      Author: lerring
 */

#include "LedDecider.h"

#include <DHTesp.h>

#include "../../events/Dht11Event.h"
#include "../../events/LedEvent.h"
#include "../../writer/Printer.h"
#include "../dht/DhtObserver.h"

LedDecider::LedDecider() {
}

LedDecider::~LedDecider() {
}

void LedDecider::update(Event* e){
	Dht11Event* dht11Event = static_cast<Dht11Event*>(e);

	this->setTemp(determineTempState(dht11Event->reading.temperature));
	this->setHumidity(determineHumidityState(dht11Event->reading.humidity));

	LedEvent ledEvent = LedEvent(this->determineWorstState());
	this->notifyObservers(&ledEvent);
}

LedState LedDecider::determineTempState(float temp) {
	if (18 <= temp && temp <= 23)
		return OKAY;
	if (16 <= temp && temp <= 27)
		return WARNING;
	return DANGER;
}

LedState LedDecider::determineHumidityState(float humidity) {
	if (35.00 <= humidity && humidity <= 60.00)
		return OKAY;
	if (25.00 <= humidity && humidity <= 75.00)
		return WARNING;
	return DANGER;
}

LedState LedDecider::determineWorstState() {
	if (this->tempState == DANGER || this->humidityState == DANGER)
		return DANGER;
	if (this->tempState == WARNING || this->humidityState == WARNING)
		return WARNING;
	return OKAY;
}


void LedDecider::setTemp(LedState newState) {
	if (this->tempState == newState)
		return;
	if (this->tempState == OKAY and this->humidityState == OKAY)
		Printer::printChangedState("from", "temp");
	if (newState == OKAY and this->humidityState == OKAY)
		Printer::printChangedState("to", "temp");
	this->tempState = newState;
}

void LedDecider::setHumidity(LedState newState) {
	if (this->humidityState == newState)
		return;
	if (this->humidityState == OKAY and this->tempState == OKAY)
		Printer::printChangedState("from", "humidity");
	if (newState == OKAY and this->tempState == OKAY)
		Printer::printChangedState("to", "humidity");
	this->humidityState = newState;
}
