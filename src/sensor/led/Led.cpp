/*
 * Led.cpp
 *
 *  Created on: 29 Jan 2020
 *      Author: lerring
 */
#include "Led.h"

// LED PINs
#define LED_PIN_R   26
#define LED_PIN_G   33
#define LED_PIN_B   32

#define MAX_LED_VALUE 99

// use first 3 channels of 16 channels (started from zero)
#define LEDC_CHANNEL_0_R  0
#define LEDC_CHANNEL_1_G  1
#define LEDC_CHANNEL_2_B  2

// use 13 bit precission for LEDC timer
#define LEDC_TIMER_13_BIT  13

// use 5000 Hz as a LEDC base frequency
#define LEDC_BASE_FREQ     5000

Led::Led() {
	ledcSetup(LEDC_CHANNEL_0_R, LEDC_BASE_FREQ, LEDC_TIMER_13_BIT);
	ledcAttachPin(LED_PIN_R, LEDC_CHANNEL_0_R);

	ledcSetup(LEDC_CHANNEL_1_G, LEDC_BASE_FREQ, LEDC_TIMER_13_BIT);
	ledcAttachPin(LED_PIN_G, LEDC_CHANNEL_1_G);

	ledcSetup(LEDC_CHANNEL_2_B, LEDC_BASE_FREQ, LEDC_TIMER_13_BIT);
	ledcAttachPin(LED_PIN_B, LEDC_CHANNEL_2_B);
}

Led::Led(int redPin, int greenPin, int bluePin){
	ledcSetup(LEDC_CHANNEL_0_R, LEDC_BASE_FREQ, LEDC_TIMER_13_BIT);
	ledcAttachPin(redPin, LEDC_CHANNEL_0_R);

	ledcSetup(LEDC_CHANNEL_1_G, LEDC_BASE_FREQ, LEDC_TIMER_13_BIT);
	ledcAttachPin(greenPin, LEDC_CHANNEL_1_G);

	ledcSetup(LEDC_CHANNEL_2_B, LEDC_BASE_FREQ, LEDC_TIMER_13_BIT);
	ledcAttachPin(bluePin, LEDC_CHANNEL_2_B);
}

Led::Led(int redPin, int bluePin, int greenPin, int rChannel, int gChannel, int bChannel, int timer, int baseFreq){
	ledcSetup(rChannel, baseFreq, timer);
	ledcAttachPin(redPin, rChannel);
	ledcSetup(gChannel, baseFreq, timer);
	ledcAttachPin(bluePin, gChannel);
	ledcSetup(bChannel, baseFreq, timer);
	ledcAttachPin(greenPin, bChannel);
}

uint32_t min(uint32_t num1, uint32_t num2) {
	if (num1 < num2) {
		return num1;
	} else {
		return num2;
	}
}

void ledcAnalogWrite(uint8_t channel, uint32_t value, uint32_t valueMax =
		MAX_LED_VALUE) {
	uint32_t duty = (LEDC_BASE_FREQ / valueMax) * min(value, valueMax);
	ledcWrite(channel, duty);
}

void Led::update(Event *e) {
	LedEvent *ledEvent = static_cast<LedEvent*>(e);
	this->changeColour(ledEvent->ledState);
	this->notifyObservers(ledEvent);
}

void Led::changeColour(LedState state) {
	switch (state) {
	case OKAY:
		ledcAnalogWrite(LEDC_CHANNEL_0_R, 0);
		ledcAnalogWrite(LEDC_CHANNEL_1_G, 99);
		ledcAnalogWrite(LEDC_CHANNEL_2_B, 0);
		break;

	case WARNING:
		ledcAnalogWrite(LEDC_CHANNEL_0_R, 80);
		ledcAnalogWrite(LEDC_CHANNEL_1_G, 25);
		ledcAnalogWrite(LEDC_CHANNEL_2_B, 0);
		break;

	case DANGER:
		ledcAnalogWrite(LEDC_CHANNEL_0_R, 99);
		ledcAnalogWrite(LEDC_CHANNEL_1_G, 0);
		ledcAnalogWrite(LEDC_CHANNEL_2_B, 0);
		break;

	case UNSET:
		break;
	}
}
