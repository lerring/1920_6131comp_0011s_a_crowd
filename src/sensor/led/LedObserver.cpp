/*
 * LedObserver.cpp
 *
 *  Created on: 3 Mar 2020
 *      Author: lerring
 */

#include "LedObserver.h"

#include "../../events/LedEvent.h"

LedObserver::LedObserver() {
}

LedObserver::~LedObserver() {
}

void LedObserver::update(Event *e){
	LedEvent* ledEvent = static_cast<LedEvent*>(e);
	this->setState(ledEvent->ledState);
}

void LedObserver::setState(LedState newState){
	this->currentLedState = newState;
}
