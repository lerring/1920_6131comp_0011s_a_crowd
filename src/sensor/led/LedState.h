/*
 * DhtState.h
 *
 *  Created on: 14 Feb 2020
 *      Author: nayaa
 */

#ifndef LEDSTATE_H_
#define LEDSTATE_H_

#include <WString.h>

enum LedState {
	OKAY, WARNING, DANGER, UNSET
};

class LedStateH {
public:
	static String asString(LedState state);
};

#endif /* LEDSTATE_H_ */
