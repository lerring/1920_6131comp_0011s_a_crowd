/*
 * DhtObserver.h
 *
 *  Created on: 18 Feb 2020
 *      Author: lerring
 */

#ifndef SENSORS_DHT11_DHTOBSERVER_H_
#define SENSORS_DHT11_DHTOBSERVER_H_

#include <DHTesp.h>

#include "../../helpers/IObserver.h"

class DhtObserver :  virtual public IObserver
{
public:
	DhtObserver();
	virtual ~DhtObserver();
	void update(Event* e);

protected:
	void setReading(TempAndHumidity currReading);
	TempAndHumidity currReading;
};

#endif /* SENSORS_DHT11_DHTOBSERVER_H_ */
