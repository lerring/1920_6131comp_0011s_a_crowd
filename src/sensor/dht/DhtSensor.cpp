/*
 * DhtSensor.cpp
 *
 *  Created on: 29 Jan 2020
 *      Author: lerring
 */

#include "DhtSensor.h"
#include "../../writer/Writer.h"

DhtSensor::DhtSensor(){

}

DhtSensor::DhtSensor( int dhtPin ) {
	dht = DHTesp();
	dht.setup(dhtPin, DHTesp::DHT11);
}


void DhtSensor::read(){
	TempAndHumidity newReading = dht.getTempAndHumidity();
	if (!isnan(newReading.temperature) && !isnan(newReading.humidity)) {
		if(this->previousReading.humidity == newReading.humidity && this->previousReading.temperature == newReading.temperature  ) {
			// if time is over 2 hours report fault
			if (millis() > this->lastChangedTime + 7200000) {
		 //7200000

				String errorMessage = "Possible DHT11 failure the readings have not changed in over 2 hours";
				Printer::print(errorMessage);
				String macAddress = WiFi.macAddress();
				unsigned long faultTime = wifii.loadTheTimes();
				String description = "Possible DHT11 connection fault";
				wifii.reportError(faultTime, macAddress,description); 
				
			}
	
		} else {
			this->previousReading = newReading;
			this->lastChangedTime = millis();

		}
	}
}

void DhtSensor::readSensor() {
	read();
	Dht11Event event = Dht11Event(this->previousReading);
	this->notifyObservers(&event);
}

/**
* Calculates the percentage difference between two numbers as a positive number 
*/ 
double percent(float oldVal, float newVal) { 
	return fabs(((oldVal - newVal) * 100) / newVal); 
} 

bool DhtSensor::warmUp(){
	read();
	int attempts = 1;
	//if no data is recieved for 10 seconds, warmUp fails
	while (!this->checkIsOK(previousReading.temperature, previousReading.humidity)){
		if (attempts > 10)
			return false;
		read();
		delay(1000);
		attempts++;
	}
	//prepare values
	TempAndHumidity* oldReading= new TempAndHumidity(previousReading);	
	double avgTempDiff;
	double avgHumDiff;
	double totalTemp=0;
	double totalHumidity=0;
	int counter=0;
	// run for 4 seconds to get a range of readings
	while (counter < 4 || (avgTempDiff > 5 || avgHumDiff > 5)) {
		delay(1000);
		counter++;
		read();
		totalTemp+=percent(oldReading->temperature,previousReading.temperature);
		totalHumidity+=percent(oldReading->humidity,previousReading.humidity);		
		avgTempDiff=totalTemp / counter;
		avgHumDiff=totalHumidity / counter; 
		oldReading = new TempAndHumidity(previousReading);
	}
	return true;
}

boolean DhtSensor::checkIsOK(float temperature, float humidity) {
	if (humidity <= 1 || isnan(humidity) )
		return false;
	if (temperature <= 1 || isnan(temperature))
  		return false;
	return true;
}
