/*
 * DhtObserver.cpp
 *
 *  Created on: 18 Feb 2020
 *      Author: lerring
 */

#include "DhtObserver.h"

#include <DHTesp.h>

#include "../../events/Dht11Event.h"
#include "../../events/LedEvent.h"
#include "../../writer/Printer.h"

DhtObserver::DhtObserver() {
}

DhtObserver::~DhtObserver(){

}

void DhtObserver::update(Event* e){
	Dht11Event* dht11Event = static_cast<Dht11Event*>(e);
	this->setReading(dht11Event->reading);
}

void DhtObserver::setReading(TempAndHumidity currReading){
	this->currReading = currReading;
}
