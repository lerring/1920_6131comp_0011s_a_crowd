/*
 * DhtSensor.h
 *
 *  Created on: 29 Jan 2020
 *      Author: lerring
 */

#ifndef DHTSENSOR_H_
#define DHTSENSOR_H_

#include <DHTesp.h>

#include "../../helpers/IObserver.h"
#include "../../helpers/Subject.h"
#include "../../events/Dht11Event.h"
#include "../../wifi/wifi.h"
#include "../../writer/Printer.h"


class DhtSensor : public Subject{
	private:
		TempAndHumidity previousReading;
		DHTesp dht;
		long lastChangedTime;
		wifi wifii;
		
		boolean checkIsOK(float temp, float humidity);
		bool isErrorStatus();
	public:
		DhtSensor();
		DhtSensor(int dhtPin);
		void read();
		void readSensor();
	    bool warmUp();
};

#endif /* DHTSENSOR_H_ */
