/*
 * PirState.cpp
 *
 *  Created on: 3 Mar 2020
 *      Author: lerring
 */

#include "PirState.h"


String PirStateH::asString(PirState state){
	switch(state){

	case OCCUPIED:
		return "Occupied";

	case UNOCCUPIED:
		return "Unoccupied";

	default:
		return "";

	}
}
