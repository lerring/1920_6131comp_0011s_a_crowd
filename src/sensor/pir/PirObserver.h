/*
 * PirObserver.h
 *
 *  Created on: 19 Feb 2020
 *      Author: lerring
 */

#ifndef SENSOR_PIR_PIROBSERVER_H_
#define SENSOR_PIR_PIROBSERVER_H_

#include "../../helpers/IObserver.h"
#include "PirState.h"
#include "../../helpers/Subject.h"
#include "../../events/PirEvent.h"

class PirObserver : virtual public IObserver {
public:
	PirObserver();
	virtual ~PirObserver();
	void update(Event* e);
protected:
	PirState currPirState;
	void setState(PirState newState);
private:
	float lastPirMovement;
	void printState(PirState state);
};

#endif /* SENSOR_PIR_PIROBSERVER_H_ */
