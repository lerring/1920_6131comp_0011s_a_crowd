/*
 * PirSensor.h
 *
 *  Created on: 29 Jan 2020
 *      Author: lerring
 */

#ifndef PIRSENSOR_H_
#define PIRSENSOR_H_

#include "PirState.h"
#include "../../events/PirEvent.h"
#include "../../helpers/Subject.h"
class PirStateManager;

class PirSensor : public Subject{
	private:
		PirState getNewState();
		int pirPin;
	public:
		PirSensor(int pirPin);
		PirSensor();
		void readSensor();
	    bool warmUp();
};

#endif /* PIRSENSOR_H_ */
