/*
 * PirState.h
 *
 *  Created on: 14 Feb 2020
 *      Author: nayaa
 */

#ifndef SENSOR_PIR_PIRSTATE_H_
#define SENSOR_PIR_PIRSTATE_H_

#include <WString.h>

enum PirState {
	OCCUPIED, UNOCCUPIED, PIR_UNSET
};

class PirStateH {
public:
	static String asString(PirState state);
};

#endif /* SENSOR_PIR_PIRSTATE_H_ */
