/*
 * PirObserver.cpp
 *
 *  Created on: 19 Feb 2020
 *      Author: lerring
 */

#include "PirObserver.h"
#include <Arduino.h>
#include "../../writer/Writer.h"
#include "PirState.h"

PirObserver::PirObserver() {
	this->lastPirMovement = 0;
	this->currPirState=OCCUPIED;
}

PirObserver::~PirObserver() {

}

void PirObserver::setState(PirState newState) {
	this->currPirState = newState;
}


void PirObserver::update(Event *e) {
	PirEvent* pirEvent = static_cast<PirEvent*>(e);
	switch (pirEvent->pirState) {
	case OCCUPIED:
		if (this->currPirState == UNOCCUPIED) {
			setState(OCCUPIED);
		}
		lastPirMovement = millis();
		break;
	case UNOCCUPIED:
		if (this->IObserver::timeDiff(lastPirMovement, 10000) && this->currPirState != UNOCCUPIED) {
			setState(UNOCCUPIED);
		}
		break;
	case PIR_UNSET:
		setState(PIR_UNSET);
		break;
	}
}


