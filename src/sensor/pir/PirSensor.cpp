/*
 * PirSensor.cpp
 *
 *  Created on: 29 Jan 2020
 *      Author: lerring
 */
#include "PirSensor.h"
#include <Arduino.h>
#include "../../writer/Writer.h"

PirSensor::PirSensor() {
}

PirSensor::PirSensor(int pirPin) {
	this->pirPin = pirPin;
	pinMode(this->pirPin, INPUT);
}

PirState PirSensor::getNewState() {
	int state = digitalRead(this->pirPin);
	if (state == HIGH)
		return OCCUPIED;
	else if (state == LOW)
		return UNOCCUPIED;
	return PIR_UNSET;
}

void PirSensor::readSensor() {
	PirState newState = this->getNewState();
	PirEvent pirEvent = PirEvent(newState);
	this->notifyObservers(&pirEvent);
}

boolean PirSensor::warmUp() {
	int sameHits = 0;
	PirState prevState = getNewState();
	for ( int i = 0; i < 10; i++){
		PirState newState = getNewState();
		if (prevState == newState){
				sameHits++;
		}
		delay(1000);
	}
	return sameHits > 5;
}
