/*
 * Button.cpp
 *
 *  Created on: 14 Feb 2020
 *      Author: nayaa
 */

#include "Button.h"

#include <esp32-hal-gpio.h>
#include <Arduino.h>

Button::Button(){
}

Button::Button( int switchPin) {
	this->switchPin=switchPin;
	pinMode(this->switchPin, INPUT);
}

ButtonState Button::getNewState() {
	if (digitalRead(this->switchPin))
	{
		return ButtonState::ON;
	}
	return ButtonState::OFF;
}

void Button::readButton() {
	ButtonEvent buttonEvent = ButtonEvent(this->getNewState());
	this->notifyObservers(&buttonEvent);
}
