/*
 * ButtonState.h
 *
 *  Created on: 14 Feb 2020
 *      Author: nayaa
 */

#ifndef SENSOR_BUTTON_BUTTONSTATE_H_
#define SENSOR_BUTTON_BUTTONSTATE_H_

enum ButtonState {
	ON, OFF
};

#endif /* SENSOR_BUTTON_BUTTONSTATE_H_ */
