/*
 * ButtonObserver.h
 *
 *  Created on: 19 Feb 2020
 *      Author: lerring
 */

#ifndef SENSOR_BUTTON_BUTTONOBSERVER_H_
#define SENSOR_BUTTON_BUTTONOBSERVER_H_

#include "../../helpers/IObserver.h"
#include "ButtonState.h"
#include "../../events/ButtonEvent.h"

class ButtonObserver : virtual public IObserver{
public:
	ButtonObserver();
	virtual ~ButtonObserver();
	void update(Event* newState);
protected:
	ButtonState currentButtonState;
	virtual void setState(ButtonState newState);

};

#endif /* SENSOR_BUTTON_BUTTONOBSERVER_H_ */
