/*
 * Button.h
 *
 *  Created on: 14 Feb 2020
 *      Author: nayaa
 */

#ifndef SENSOR_BUTTON_BUTTON_H_
#define SENSOR_BUTTON_BUTTON_H_

#include "ButtonState.h"
#include "../../events/ButtonEvent.h"
#include "../../helpers/Subject.h"

class Button : public Subject{
	private:
		long lastChange;
		int switchPin;
		ButtonState getNewState();
	public:
		Button();
		Button(int switchPin);
		void readButton();
		bool warmUp();
};

#endif /* SENSOR_BUTTON_BUTTON_H_ */
