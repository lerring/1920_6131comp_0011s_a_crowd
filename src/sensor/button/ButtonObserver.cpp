/*
 * ButtonObserver.cpp
 *
 *  Created on: 19 Feb 2020
 *      Author: lerring
 */

#include "ButtonObserver.h"
#include <Arduino.h>
ButtonObserver::ButtonObserver() {
	this->setState(ButtonState::OFF);
}

ButtonObserver::~ButtonObserver() {
}

void ButtonObserver::update(Event *e) {
	ButtonEvent* buttonEvent = static_cast<ButtonEvent*>(e);

	//Only set state if something has changed
	switch(this->currentButtonState){
	case ButtonState::ON:
		if (buttonEvent->buttonState == ButtonState::OFF)
			this->setState(buttonEvent->buttonState);
		break;
	case ButtonState::OFF:
		if (buttonEvent->buttonState == ButtonState::ON){
			this->setState(buttonEvent->buttonState);
		}
		break;
	}
}

void ButtonObserver::setState(ButtonState newState){
	this->currentButtonState = newState;
}

