/*
 * Printer.h
 *
 *  Created on: 27 Feb 2020
 *      Author: lerring
 */

#ifndef WRITER_PRINTER_H_
#define WRITER_PRINTER_H_

#include <WString.h>

class Printer {
public:
	Printer();
	virtual ~Printer();
	static void printIsOK();
	static void printChangedState(String toFrom, String due);
	static void print(String toPrint);
	static void printCycle(int cycle);
};

#endif /* WRITER_PRINTER_H_ */
