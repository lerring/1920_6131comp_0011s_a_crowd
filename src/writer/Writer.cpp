/*
 * Writer.cpp
 *
 *  Created on: 1 Feb 2020
 *      Author: emma
 */

#include "Writer.h"

#include "../helpers/IObserver.h"
#include "Printer.h"

Writer::Writer(){

}
Writer::Writer(int sd_CsPin){
	wiFi.initialiseWifi();
	offset = wiFi.loadTheTimes();
	sdCard = SdCard(sd_CsPin, offset);
}


void Writer::setState(PirState newState) {
	if(newState==PIR_UNSET)
		reportError("Failed reading from PIR");
	else 
		GodObserver::setState(newState);
}

void Writer::setReading(TempAndHumidity tmp){
	if(tmp.humidity == NULL || tmp.temperature == NULL)
		reportError("Failed reading from DHT");
	else 
		GodObserver::setReading(tmp);
}

void Writer::setState(ButtonState newState) {
	GodObserver::setState(newState);
	if (this->currentButtonState == ButtonState::ON) {
		this->count++;
	    int arrSize = sizeof(this->cycleTimesSeconds) / sizeof(this->cycleTimesSeconds[0]);
		if (this->count % arrSize == 0) {
			this->count = 0;
		}
		this->writeDelay=(this->cycleTimesSeconds[count] >= 30) ? this->cycleTimesSeconds[this->count] : 30;
		Printer::printCycle(this->cycleTimesSeconds[this->count]);
	}
}

void Writer::writeToVolatile(){
	if (!this->IObserver::timeDiff(this->lastWriteToVolatile,this->cycleTimesSeconds[this->count] * 1000)){
		return;
	}
	this->lastWriteToVolatile = millis();
	serverQueue.push_back("{\"Temp\": "     + String(this->currReading.temperature) +
					 ", \"Humidity\": "     + String(this->currReading.humidity) +
					 ", \"PirState\": \""   + PirStateH::asString(this->currPirState) +
					 "\", \"LedState\": \"" + LedStateH::asString(this->currentLedState) +
					 "\", \"time\": "       + ((millis() / 1000) + offset) +
					 ", \"ArduinoMac\": \"" + WiFi.macAddress() +
					 "\"}");
}

void Writer::writeToSerial(){
       String toWrite = "Temp: " + String(this->currReading.temperature) +
                       " Humidity: " + String(this->currReading.humidity) +
                       " PirState: " + PirStateH::asString(this->currPirState) +
                       " LedState: " + LedStateH::asString(this->currentLedState);
       Printer::print(toWrite);
}

void Writer::writeToSd(){
	sdCard.writeToSd(sdCardQueue);
}

void Writer::writeToServer(){
	if (!this->IObserver::timeDiff(this->lastWriteToServer,this->writeDelay)){
		return;
	}

	wiFi.sendToServer(serverQueue);
	for (int i=0; i < serverQueue.size(); i++ ) {
		sdCardQueue.push_back(serverQueue[i]);
	}
	//write method to buffer
	serverQueue.clear();

	this->lastWriteToServer = millis();
}

void Writer::reportError(String description){
	wiFi.reportError(((millis() / 1000) + offset), WiFi.macAddress(), description);	
}