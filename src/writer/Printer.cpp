/*
 * Printer.cpp
 *
 *  Created on: 27 Feb 2020
 *      Author: lerring
 */

#include "Printer.h"

#include "Arduino.h"

void Printer::print(String toPrint){
	//if timediff matches setting
	Serial.println(toPrint);
}

void Printer::printIsOK() {
	print("OK");
}

void Printer::printChangedState(String toFrom, String due){
	print("LED_SENSOR CHANGED_STATE "+toFrom+" GREEN due to "+due);
}

void Printer::printCycle(int cycleTime){
	print("Cycle time updated to: " + String(cycleTime));
}
