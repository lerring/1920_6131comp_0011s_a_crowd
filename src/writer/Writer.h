/*
 * Writer.h
 *
 *  Created on: 1 Feb 2020
 *      Author: emma
 */


#ifndef WRITER_H_
#define WRITER_H_

#include <esp32-hal.h>
#include <WString.h>
#include <vector>

#include "../helpers/GodObserver.h"
#include "../sensor/button/ButtonState.h"
#include "../sensor/pir/PirState.h"
#include "../sdcard/SdCard.h"
#include "../wifi/wifi.h"

using namespace std;
#include "DHTesp.h"

class Writer : public GodObserver{
private:
	vector<String> serverQueue;
	vector<String> sdCardQueue;
	SdCard sdCard;
	int cycleTimesSeconds[6] = { 5, 10, 30, 60, 120, 300 };
    int writeDelay = 30;
	int count = 0;
	float lastWriteToServer = millis();
	float lastWriteToVolatile = millis();
	wifi wiFi;
	unsigned long int offset = 0;

public:
	Writer();
	Writer(int sd_CsPin);
	void writeToVolatile();
	void writeToSerial();
	void writeToServer();
	void writeToSd();
	void setState(PirState newState);
	void setState(ButtonState newState);
	void setReading(TempAndHumidity newReading);
	void reportError(String description);
};

#endif /* WRITER_H_ */
